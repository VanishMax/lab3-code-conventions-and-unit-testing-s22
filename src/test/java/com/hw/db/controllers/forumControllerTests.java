package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Forum;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

class forumControllerTests {
    private User auth;
    private Forum toCreate;

    @BeforeEach
    @DisplayName("forum creation test")
    void init() {
        auth = new User("user", "example@domain.com", "example", "title");
        toCreate = new Forum(1282, "example", 5, "title", "example");
    }

    @Test
    @DisplayName("Create forum")
    void createForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("user")).thenReturn(auth);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(toCreate), controller.create(toCreate), "Result for succeeding forum creation");
            }
            assertEquals(auth, UserDAO.Search("user"));
        }
    }
}
package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class threadControllerTests {
    private threadController controller;
    
    private User user;
    private final String username = "username";

    private Post post;
    private final String slug = "slug";
    private List<Post> postList;
    
    private Thread thread;
    private final int threadId = 1282;

    @BeforeEach
    void init() {
        user = new User(username, "example@domain.com", "Example", "example");
        
        postList = new ArrayList<>();
        Timestamp now = Timestamp.from(Instant.now());
        final String forum = "forum";
        post = new Post(username, now, forum, "example", 0, 0, false);
        postList.add(post);

        controller = new threadController();
        thread = new Thread(username, now, forum, "example message", slug, "Thread Title", 3);
        thread.setId(threadId);
    }

    @Test
    @DisplayName("Create post")
    void createPost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                userMock.when(() -> UserDAO.Info(username)).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postList), controller.createPost(slug, postList));
                assertEquals(thread, ThreadDAO.getThreadBySlug(slug));
            }
        }
    }

    @Test
    @DisplayName("Fetch posts list")
    void fetchPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getPosts(threadId, 1, 100, "flat", false)).thenReturn(postList);
            threadMock.when(() -> ThreadDAO.getPosts(threadId, 0, 100, "flat", false)).thenReturn(Collections.emptyList());

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(postList), controller.Posts(slug, 1, 100, "flat", false));
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()), controller.Posts(slug, 0, 100, "flat", false));
        }
    }

    @Test
    @DisplayName("Get thread by slug")
    void getThreadBySlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(controller.CheckIdOrSlug(slug), thread);
            assertNull(controller.CheckIdOrSlug("none"));
        }
    }

    @Test
    @DisplayName("Get thread by ID")
    void getThreadById() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
            assertEquals(controller.CheckIdOrSlug(Integer.toString(threadId)), thread);
            assertNull(controller.CheckIdOrSlug("someid"));
        }
    }

    @Test
    @DisplayName("Update thread")
    void updateThread() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            final String slug2 = "slug 2";
            final Timestamp ts2 = Timestamp.valueOf("2025-05-12 00:00:00");
            final int threadId2 = 100;
            final Thread thread2 = new Thread(username, ts2, "forum test", "example", slug2, "title example", 4);

            thread2.setId(threadId2);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug2)).thenReturn(thread2);
            threadMock.when(() -> ThreadDAO.getThreadById(threadId2)).thenReturn(thread2);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

            threadMock.when(() -> ThreadDAO.change(thread, thread2)).thenAnswer((invocationOnMock) -> {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug2)).thenReturn(thread2);
                return null;
            });

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread2), controller.change(slug2, thread2));
            assertEquals(thread2, controller.CheckIdOrSlug(slug2));
        }
    }

    @Test
    @DisplayName("Fetch a thread")
    void fetchThread() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.info(slug));

            threadMock.when(() -> ThreadDAO.getThreadBySlug("none")).thenThrow(new DataAccessException("") {});
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null).getStatusCode(), controller.info("none").getStatusCode());
        }
    }

    @Test
    @DisplayName("Create vote")
    void createVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                final Vote vote = new Vote(username, 1);
                userMock.when(() -> UserDAO.Info(username)).thenReturn(user);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                threadMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.createVote(slug, vote));
            }
        }
    }
}